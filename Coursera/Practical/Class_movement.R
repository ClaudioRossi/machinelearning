#################################
### dataset movements sensors ###
#################################
require(Hmisc)
library(ggplot2)
library(caret)
library(caret)


myDir="C:\\Users\\Claudio\\Dropbox\\Boella\\machinelearning\\Coursera\\Practical" #solo windows

WD <- getwd()
print(WD)

setwd(myDir)
print(getwd())

#Reading local file - read.table() | read.csv() | read.csv2()
#sensorsData <- read.table("./dataset-har-PUC-Rio-ugulino.csv", sep=";", header=TRUE)
sensorsData = read.csv2(file="dataset-har-PUC-Rio-ugulino.csv", header = TRUE, sep=';')
head(sensorsData)
dim(sensorsData)
summary(sensorsData)

#Data manipulation###########################
#remove name because it is not correlated
sensorsData=sensorsData[,2:19]
summary(sensorsData)
#translate Man and Women into integer
sensorsData$gender =ifelse(sensorsData$gender=="Man", 0, 1)
table(sensorsData$gender)
#force z4 to be a number - apparently it is not
sensorsData$z4=as.numeric(as.character(sensorsData$z4))
#substitute NA with mean
sensorsData$z4[is.na(sensorsData$z4)] <- mean(sensorsData$z4[!is.na(sensorsData$z4)])
dim(sensorsData)
####################################

#Training and testing set #####################################################
inTrain <- createDataPartition(y=sensorsData$class, p=0.2, list=FALSE)
training <- sensorsData[inTrain,]
testing <- sensorsData[-inTrain,]
dim(training)
dim(testing)
summary(training)
###############################################################################

#featurePlot too slow - blocking
#featurePlot(x=training[,c("gender","age","how_tall_in_meters","weight","body_mass_index")],
#            y = training$class,
#            plot="pairs")

# Pre processing #################################################

#Use pre-process -center and scale and PCA to aggregate correlated features - TBC
#preCenterScale <- preProcess(training[,-58],method=c("center","scale"))

#manual principal component analysis - TBC
#trainPr <- training[,-18]
#summary(trainPr)
#prComp <- prcomp(trainPr)
#prComp

preProc <- preProcess(training[,-18], method="pca", pcaComp=10)
preProc

trainPC <- predict(preProc,training[,-58])
summary(trainPC)
#now test witht he test set
testPC <- predict(preProc, testing[,-58])
summary(testPC)
###################################################################


#Train and Evaluation #############################################

#Train control parameter
#fitControl <- trainControl(method = "LGOCV", allowParallel=TRUE)

#Try to model with rpart - does not work good - accuracy of 0.63
#modelFit <- train(training$class ~ ., method="rpart", data=trainPC )
#confusionMatrix(testing$class,predict(modelFit,testPC))

#Random forest long
#mtryGrid <- expand.grid(mtry = 100)
#modFit <- train(class ~ ., data=trainPC, method="rf", ntree = 500, keep.forest = FALSE, trControl = fitControl, importance=TRUE )
modFit <- train(class ~ ., data=trainPC, method="rf" )
modFit
confusionMatrix(testing$class,predict(modFit,testPC))
###################################################################
