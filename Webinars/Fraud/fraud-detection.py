#!/usr/bin/env python
# -*- coding: utf-8 -*-


# coding: utf-8

## Detecting Credit Card Fraud

# In[1]:

import graphlab as gl


# In[2]:

data = gl.SFrame('fraud_detection.sf')


#### Visualize the data

# In[3]:

data.head(3)


# In[4]:

len(data)


# In[5]:

data.show()


#### Create new features

##### Date features

# In[6]:

def extract_date(x):
    splits = x.split('.')
    return {
        'day' : splits[0],
        'month' : splits[1],
        'year' : splits[2]
    }

date_columns = data['transaction date'].apply(extract_date)
data.add_columns(date_columns.unpack(column_name_prefix='transaction'))
data.remove_column('transaction date')


# In[7]:

from datetime import datetime

data['transaction week day'] = data.apply(lambda x: datetime(int(x['transaction.year']),
                                                             int(x['transaction.month']),
                                                             int(x['transaction.day'])).weekday())

data['transaction week day'] = data['transaction week day'].astype(str)


##### Indicator features

# In[8]:

data['same country'] = data.apply(lambda x: x['customer country'] == x['business country'])
data['same country'] = data['same country'].astype(str)

data['same person'] = data.apply(lambda x: x['customer'] == x['cardholder'])
data['same person'] = data['same person'].astype(str)

data['expiration near'] = data.apply(lambda x: x['credit card expiration year'] == x['transaction.year'])
data['expiration near'] = data['expiration near'].astype(str)


##### Count features

# In[9]:

counts = data.groupby('transaction id', {'unique cards per transaction' : gl.aggregate.COUNT_DISTINCT('credit card number'),
                                         'unique cardholders per transaction' : gl.aggregate.COUNT_DISTINCT('cardholder'),
                                         'tries per transaction' : gl.aggregate.COUNT()})
data = data.join(counts)


# In[10]:

print 'Number of columns', len(data.column_names())


#### Split data to train and test sets

# In[11]:

split = data.apply(lambda x: 1 if ((int(x['transaction.month']) >= 6) and (int(x['transaction.year']) >= 2015)) else 0)

train = data[split == 0]
test = data[split == 1]

print 'Train len', len(train)
print 'Train fraud', (train['fraud'] == 'yes').sum() / float(len(train))

print 'Test len', len(test)
print 'Test fraud', (test['fraud'] == 'yes').sum() / float(len(test))


#### Create model to predict if a given transaction is fraudulent

##### Logistic Regression baseline

# In[13]:

logreg_model = gl.logistic_classifier.create(train,
                                             target='fraud',
                                             validation_set=None)


# In[14]:

print 'Logistic Regression AUC', logreg_model.evaluate(test)['auc']
print 'Logistic Regression Confusion Matrix\n', logreg_model.evaluate(test)['confusion_matrix']


##### Boosted Trees Classifier

# In[15]:

boosted_trees_model = gl.boosted_trees_classifier.create(train, 
                                                         target='fraud',
                                                         validation_set=None)


# In[16]:

print 'Boosted trees AUC', boosted_trees_model.evaluate(test)['auc']
print 'Boosted trees Confusion Matrix\n', boosted_trees_model.evaluate(test)['confusion_matrix']


# In[17]:

boosted_trees_model = gl.boosted_trees_classifier.create(train, 
                                                         target='fraud',
                                                         validation_set=None,
                                                         max_iterations=40,
                                                         max_depth=8)


# In[18]:

print 'Boosted trees AUC', boosted_trees_model.evaluate(test)['auc']
print 'Boosted trees Confusion Matrix\n', boosted_trees_model.evaluate(test)['confusion_matrix']


# In[19]:

boosted_trees_model.get_feature_importance()


#### Deploying the model into a resilient & elastic service

# In[ ]:

gl.aws.set_credentials(<your public key>,
                       <your private key>)

state_path = 's3://gl-demo-usw2/predictive_service/demolab/ps-1.7.1'
deployment = gl.deploy.predictive_service.load(state_path)


# In[ ]:

deployment.add('fraud', boosted_trees_model)


# In[ ]:

deployment.apply_changes()


# In[ ]:

deployment.show()


#### RESTfully query the service

# In[ ]:

deployment.query('fraud', method='predict', data=test[0])

