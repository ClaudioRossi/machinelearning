from mrjob.emr import EMRJobRunner
EMRJobRunner._check_input_exists = lambda *args: True

from mrjob.job import MRJob
from mrjob.protocol import JSONValueProtocol


class MRTweetWordCount(MRJob):
    INPUT_PROTOCOL = JSONValueProtocol

    def mapper(self, __, tweet):
        text = tweet.get('text', '')
        for word in text.split():
            if word.startswith('#'):
                yield word, 1

    def reducer(self, key, counts):
        s = sum(counts)
        if s > 10:
            yield key, s


if __name__ == '__main__':
    MRTweetWordCount.run()
