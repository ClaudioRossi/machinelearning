caps = {}
import csv
with open('listacomuni.txt', encoding='latin-1') as file:
    next(file)
    reader =csv.reader(file, delimiter=';')
    for row in reader:
        caps[row[5]] = row[0:2]

import collections

import pymongo
db = pymongo.MongoClient().consel

def get_comune(cap):
	if loan[cap_kind] == '99999': return None
	#print('localizing', cap)
	if cap in caps:
		return caps[cap]
	else:
		if int(cap[2]) % 2:
			cap4 = cap[:4]+'x'
			if cap4 in caps:
				return caps[cap4]
			cap3 = cap[:3]+'xx'
			if cap3 in caps:
				return caps[cap3]
			else:
				return None
		else:
			province_cap = cap[:2]+str(int(cap[2])+1)+'00'
			return get_comune(province_cap)
		
for cap in db.loans.distinct('employer_cap'):
	if cap == '99999':
		continue
	cap = "%05d" % int(cap)
	#print("CAP", cap)
	comune = get_comune(cap)
	#print("COMUNE", comune	)
