# Machine Learning #

This repository contains material related to Machine Learning.
Different Coursera Courses are contained, namely:

* Machine Learning Foundations: A Case Study Approach by University of Washington
* Practical Machine Learning
* Getting and Cleaning Data

Also the material of the course BIGDIVE is included
